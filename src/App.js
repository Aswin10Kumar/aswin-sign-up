import { Component, Fragment } from 'react'
import SignIn from './component/SignIn'
import Welcome from './component/Welcome'
// import { Route, BrowserRouter } from 'react-router-dom'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      fname: '',
      lname: '',
      isLoggedIn: false
    }
  }


  informationHandler = ({ fname, lname }) => {
    console.log(fname, lname)
    this.setState({
      fname,
      lname,
      isLoggedIn: true
    }/*,()=>{console.log(this.state.fname,this.state.lname)}*/)
  }

  render() {
    return (
      <Fragment>
        {!this.state.isLoggedIn && <SignIn getInfo={this.informationHandler.bind(this)} />}
        {this.state.isLoggedIn && <Welcome name={this.state.fname} />}
      </Fragment>
    )
  }
}

export default App

