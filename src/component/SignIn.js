import React, { Component } from 'react'
import style from './SignIn.module.css'

class SignIn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            fname: false,
            lname: false,
            email: false,
            pass: false,
            fnameErr: false,
            lnameErr: false,
            emailErr: false,
            passErr: false,
            confirmPassErr: false,
            isTouched: false
        }
    }

    fnameHandler = (event) => {
        const name = event.target.value;
        const nameValidation = name.trim().length !== 0;

        if (nameValidation) {
            this.setState({ fnameErr: false, fname: name })
        } else {
            this.setState({ fnameErr: true });
        }
    }

    lnameHandler = (event) => {
        const name = event.target.value;

        if (name.trim().length !== 0) {
            this.setState({ lnameErr: false, lname: name})
        } else {
            this.setState({ lnameErr: true });
        }

    }

    emailHandler = (event) => {
        const email = event.target.value;
        let emailValidation = email.includes("@") && email.includes('.', email.indexOf('@') + 1);
        const pattern = /^[^ ]+@[^ ]+\.[a-z]{2,}$/;
        console.log(email.match(pattern))
        if (emailValidation) {
            console.log("email")
            this.setState({ emailErr: false, email: email })
        } else {
            this.setState({ emailErr: true});
        }

    }

    passHandler = (event) => {
        const password = event.target.value;
        let pattern= /^(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[!@#$%^&*])(?=.{8,})/;
        console.log(password.match(pattern),password);
        if (password.length > 8) {
            this.setState({ passErr: false, pass: password})
            // console.log(password)
        }
        else {
            this.setState({ passErr: true })
        }
    }

    confirmHandler = (event) => {
        const pwd = event.target.value;
        if (pwd === this.state.pass) {
            this.setState({ confirmPassErr: false })
        }
        else {
            this.setState({ confirmPassErr: true })
        }
    }

    submitHandler = (event) => {
        event.preventDefault();
        const validateForm = this.state.fname && this.state.lname && this.state.pass && this.state.email && this.state.confirmPass;
        if (validateForm === undefined) {
            this.props.getInfo({ fname: this.state.fname, lname: this.state.lname })
            this.setState({
                fname: '',
                lname: '',
                email: '',
                pass: ''
            })
        }
    }

    render() {
        return (
            <div className={style["form-layout"]}>
                <h1>Sign Up</h1>
                <form onSubmit={this.submitHandler}>
                    <input type="text" placeholder="First name" onBlur={this.fnameHandler}></input> <br />
                    {this.state.fnameErr && (<div className={style.err}>Enter proper name</div>)}
                    <input type="text" placeholder="Last name" onBlur={this.lnameHandler}></input><br />
                    {this.state.lnameErr && <div className={style.err}>Enter proper name</div>}
                    <input type="text" placeholder="example@yahoo.com" onBlur={this.emailHandler}></input><br />
                    {this.state.emailErr && <div className={style.err}>Enter valid email ID</div>}
                    <input type="password" placeholder="Password" onBlur={this.passHandler}></input><br />
                    {this.state.passErr && <div className={style.err}>Password min-length is 8</div>}
                    <input type="password" placeholder="Confirm Password" onBlur={this.confirmHandler}></input><br />
                    {this.state.confirmPassErr && <div className={style.err}>Password not matching</div>}
                    <div className={style.btn}> <button type="submit">Submit</button></div>
                </form>
            </div>
        )
    }
}

export default SignIn

// contain alphaNumeric and special character(!@#$%*)